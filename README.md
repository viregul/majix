# Script Majix

Script magique de mise à jour d'une distribution Linux
Ce script propose de mettre à jour votre distribution à l'aide de paquets choisis :
* Outils pour langue française : pages de langues, manuels, dictionnaires...
* Petits outils accessoires : editeur, mots de passe, gestion de fichiers...
* Internet : Navigation et messagerie
* Bureautique : carte heuristique, mise en page...
* Outils Retouche d'images : retouche fine et visionneuse, capture d'écran...
* Outils Retouche Audio Vidéo : son, video, gravage, encodage...
* Jeux et Education : petits jeux
* Polices de caractères : polices supplémentaires
* Visionneur de bureau : avec ppa (dépots complémentaires)
* Libreoffice : dernière version avec ppa
* Capture d'écran : avec ppa
* Paquets non-libres : pour lecture de DVD, formats non libres...

Ce script propose aussi d'ajouter ces propres paquets et peut être modifié ou adapté à vos besoins.

## Installation

Télécharger le fichier majix.sh
Lancer la commande :

sudo ./majix.sh

Il proposera à la fin d'appliquer les changements ou d'annuler mais avec la crétion d'un script manuel final que vous pourrez modifier, adapter...

Toutes les étapes sont proposées avec une interface graphiques

## Options
Il est possible de lancer le script avec 2 options :
`--free` : ne propose que les paquets libres
`--all` : valide par défaut tous les paquets proposés (installation rapide)

Ces options ne seront appliquées qu'après la validation finale.


