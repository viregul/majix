#!/bin/bash

#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#

# Script mise à jour d'une distribution Linux pour la rendre conforme aux besoins
# Authors : Francois Audirac <francois@webaf.net>, Laurent Poussier <poussier.laurent@wanadoo.fr>
# version 2.0

#Couleurs bash
RESTORE='\033[0m'
RED='\033[00;31m'
GREEN='\033[00;32m'
YELLOW='\033[00;33m'
BLUE='\033[00;34m'
PURPLE='\033[00;35m'
CYAN='\033[00;36m'
LIGHTGRAY='\033[00;37m'

# Variables
PPA_INITIAUX="phoerious/keepassxc shutter/ppa"
my_selection_paquets_a_installer=""
my_selection_ppa_a_installer=$PPA_INITIAUX
VERSION="2.0"
DEBUG="" #remplacer par -s ou --dry-run
MAJIX_COMMANDE="majix_commande.sh"
PARAMS=$*

PURGE_PAQUETS_FR="firefox-locale-de firefox-locale-en firefox-locale-es firefox-locale-it firefox-locale-pt firefox-locale-ru firefox-locale-zh-hans thunderbird-locale-de thunderbird-locale-en thunderbird-locale-en-gb thunderbird-locale-en-us thunderbird-locale-es thunderbird-locale-es-ar thunderbird-locale-es-es thunderbird-locale-it thunderbird-locale-pt thunderbird-locale-pt-br thunderbird-locale-pt-pt thunderbird-locale-ru thunderbird-locale-zh-cn thunderbird-locale-zh-hans thunderbird-locale-zh-hant thunderbird-locale-zh-tw hunspell-* hyphen-* mythes-* libreoffice-help-de libreoffice-help-en-gb libreoffice-help-en-us libreoffice-help-es libreoffice-help-it libreoffice-help-pt libreoffice-help-pt-br libreoffice-help-ru libreoffice-help-zh-cn libreoffice-help-zh-tw  libreoffice-l10n-de libreoffice-l10n-en-gb libreoffice-l10n-en-za libreoffice-l10n-es libreoffice-l10n-it libreoffice-l10n-pt libreoffice-l10n-pt-br libreoffice-l10n-ru libreoffice-l10n-zh-cn libreoffice-l10n-zh-tw"

# Suppression de polices étrangeres
PURGE_PAQUETS_FONTS="fonts-tlwg-* fonts-lohit-* fonts-orya-extra fonts-pagul fonts-sahadeva fonts-samyak-* fonts-sarai fonts-sil-abyssinica fonts-sil-padauk fonts-takao-pgothic fonts-takao-gothic fonts-telu* fonts-khmeros-core* fonts-tibetan-machine* fonts-deva-extra* fonts-kacst* fonts-kacst-one* fonts-kalapi* fonts-nakula* fonts-nanum* fonts-navilu* fonts-noto* fonts-noto-cjk* fonts-noto-hinted* fonts-noto-mono* fonts-noto-unhinted* fonts-gubbi fonts-gujr-extra fonts-guru-extra fonts-lao fonts-lklug-sinhala fonts-wqy-microhei fonts-beng-extra"


function init_majix {
    # Test si droits admin présents
    MOI=`whoami`
    if [[ ! $MOI == "root" ]];
    then
	    echo -e "! Execution réservée aux ${RED} administrateurs${RESTORE} !\n Il faut relancer le programme avec la commande :\n sudo ${RED}./majix.sh ${RESTORE}"
	    exit 0;
    fi

    # test de présence de Yad
    if [[ ! -e /usr/bin/yad ]];
    then
	apt-get -y install yad
    fi

    # Creation du fichier MAJIX_COMMANDE
    if [[ -e $MAJIX_COMMANDE ]]; then rm -rf $MAJIX_COMMANDE ; touch $MAJIX_COMMANDE ; chmod +x $MAJIX_COMMANDE; fi
}


function accueilinfo {
    #Affiche le message d'accueil principal
    yad --center --question --borders=20 --title "Bienvenue sur Linux Majix" --image=system-software-install --height=200 --width=500 --text="<b>Majix</b> est un programme de Mise À Jour de votre <b>distribution Linux</b>. Ce programme va vous permettre de mettre à jour votre système en y installant rapidement des <b>paquets recommandés</b>.\n \n Ce programme (v $VERSION) sous licence GPL v3.0 vous est offert par l'association VireGUL : https://viregul.fr \n \n Ce programme va vous proposer une liste de programmes à installer (non présents) et effectuera une mise à jour complète. \n La validation finale ne sera lancée qu'à la fin de votre sélection." 2>/dev/null
    if [[ ! $? == 0 ]]; then
	exit 0;
    fi

}

function creechaineYadpaquets {
    # listepaquets vaut la liste des paquets séparés par espace
    # Cette fonction sert à insérer TRUE dans la liste des paquets
    CHAINEYADPAQUETS=""
    for PAQUET in "$@"
    do
        CHAINEYADPAQUETS=$CHAINEYADPAQUETS"TRUE "$PAQUET" ";
    done
    echo $CHAINEYADPAQUETS
}


function afficheYadprogramme {
    # Affiche la fenêtre Yad avec les paramètres de liste des programmes
    CHTRUEPAQUETS=`creechaineYadpaquets $@`
    PAQUETS=$(yad  --title="Programmes à installer" --center --height=$VALHAUTEUR --width=400  --text="Programmes : <b>$TXTPAQUETS</b>" --list --checklist --column="Install" --column="Programmes" $CHTRUEPAQUETS --separator=" "  --button="Aucun":1 --button=gtk-ok:0 2>/dev/null);
    echo $PAQUETS
}

function renvoiepaquets {
    # Fonction qui teste les paquets déjà présents et renvoie la liste des paquets installables"
    LISTENOUVEAUXPAQUETS_A_INSTALLER=""
    for PAQUETPRESENT in $LISTEPAQUETS;
    do
        RESULTAT=`dpkg -l "$PAQUETPRESENT" 2> /dev/null | grep '^ii'`
        if [[ -z $RESULTAT ]];
        then
            LISTENOUVEAUXPAQUETS_A_INSTALLER="$LISTENOUVEAUXPAQUETS_A_INSTALLER $PAQUETPRESENT"
        fi
    done
    HAUTEUR=`echo $LISTENOUVEAUXPAQUETS_A_INSTALLER|wc -w`
    VALHAUTEUR=$((($HAUTEUR*28)+120))
    if [[ -n $LISTENOUVEAUXPAQUETS_A_INSTALLER ]];
        then
            PAQUETS=`afficheYadprogramme "$LISTENOUVEAUXPAQUETS_A_INSTALLER"`
    fi
    #On purge la liste des paquets des TRUE renvoyés en trop
    LISTEPACK=""
    for PACK in $PAQUETS; do
	if [[  $PACK != TRUE ]]; then
	    LISTEPACK="$LISTEPACK $PACK";
	fi
    done
	# On renvoie la liste des paquets sans le "TRUE"
    echo $LISTEPACK
    
}

function test_all {
    #Vérifie si --all est dans les paramètres
    if [[ -n `echo "$PARAMS"| grep -- "--all"` ]]; then	return 0; else return 1; fi
}

function test_free {
    #Vérifie si --all est dans les paramètres
    if [[ -n `echo "$PARAMS"| grep -- "--free"` ]]; then return 0; else return 1; fi
}


function ajoutpaquetsmanuels {
    # Ajout manuel de paquets
    PAQUETSMANUELS=$(yad  --entry --borders=20 --title="Installer des paquets supplémentaires" --text="Vous pouvez installer des paquets supplémentaires en séparant les noms avec espace" --width=500 --button="Aucun":1 --button=gtk-ok:0 2>/dev/null)
    if [[ ! -z $PAQUETSMANUELS ]];
    then
	echo $PAQUETSMANUELS
    fi
}


function confirmationfinale {
    # Message confirmation finale pour installation
    REPONSE=$(yad --question --borders=20 --text="Attention, vous allez procéder à la <span color='#FF0000'>validation de la mise à jour</span>. Cette mise à jour désinstallera aussi un certain nombre de paquets de langue, polices de caractères inutiles. Voulez-vous <b>confirmer</b> cette mise à jour ?" --width=500 --button=gtk-quit:1 --button=gtk-ok:0 2>/dev/null)
    if [[ ! $? == 0 ]];
    then
        echo -e "
Vous avez choisi d'${RED}annuler la procédure ${RESTORE}, mais vous pouvez
toujours modifier le script final ${PURPLE} $MAJIX_COMMANDE ${RESTORE}
et l'exécuter ensuite avec la commande :
sudo bash ${GREEN} $MAJIX_COMMANDE ${RESTORE}"
    	exit 0;
    else
	DATE=$(date +%Y%m%d-%H%M)
        bash $MAJIX_COMMANDE |tee "log_install$DATE.txt";
    fi
}

function finsortie {
    # Messsage de sortie
    # Message final
    yad  --width=400 --info --borders=20 --title="Installation terminée" --text="<b>L'installation est terminée !</b> \n \n Vous pouvez nous retrouver sur https://viregul.fr\n\nMerci." 2>/dev/null
}


##### Partie principale ########

init_majix
# Initialisation des variables, des dossier, fichiers

accueilinfo
# Message d'accueil
test_all; TEST_ALL=$?
test_free; TEST_FREE=$?
# echo -e "TEST_ ALL $TEST_ALL, TEST_FREE $TEST_FREE\n"

TXTPAQUETS="Outils pour langue française"
LISTEPAQUETS="mythes-fr hyphen-fr hunspell-fr manpages-fr manpages-fr-extra hunspell-fr-classical language-pack-fr doc-linux-fr-text fortunes-fr"
if [[ "$TEST_ALL" -eq 0 ]]; then PAQUETS=$LISTEPAQUETS; else PAQUETS=`renvoiepaquets $LISTEPAQUETS`; fi
my_selection_paquets_a_installer=$my_selection_paquets_a_installer" "$PAQUETS

TXTPAQUETS="Petits accessoires"
LISTEPAQUETS="p7zip geany geany-plugins fslint vim grsync meld tomboy keepassxc bleachbit pwgen pass typecatcher gparted synaptic whois sysinfo"
if [[ "$TEST_ALL" -eq 0 ]]; then PAQUETS=$LISTEPAQUETS; else PAQUETS=`renvoiepaquets $LISTEPAQUETS`; fi
my_selection_paquets_a_installer=$my_selection_paquets_a_installer" "$PAQUETS

TXTPAQUETS="Internet "
LISTEPAQUETS="thunderbird thunderbird-locale-fr firefox firefox-locale-fr pidgin"
if [[ "$TEST_ALL" -eq 0 ]]; then PAQUETS=$LISTEPAQUETS; else PAQUETS=`renvoiepaquets $LISTEPAQUETS`; fi
my_selection_paquets_a_installer=$my_selection_paquets_a_installer" "$PAQUETS

TXTPAQUETS="Bureautique"
LISTEPAQUETS="freeplane scribus dia dia-shapes xournal"
if [[ "$TEST_ALL" -eq 0 ]]; then PAQUETS=$LISTEPAQUETS; else PAQUETS=`renvoiepaquets $LISTEPAQUETS`; fi
my_selection_paquets_a_installer=$my_selection_paquets_a_installer" "$PAQUETS

TXTPAQUETS="Retouche et capture d'images"
LISTEPAQUETS="imagemagick pinta gimp gimp-help-fr gthumb inkscape imagemagick shutter"
if [[ "$TEST_ALL" -eq 0 ]]; then PAQUETS=$LISTEPAQUETS; else PAQUETS=`renvoiepaquets $LISTEPAQUETS`; fi
my_selection_paquets_a_installer=$my_selection_paquets_a_installer" "$PAQUETS

TXTPAQUETS="Audio et Vidéo"
LISTEPAQUETS="audacity audacious kdenlive soundconverter easytag transmageddon handbrake ffmpeg"
if [[ "$TEST_ALL" -eq 0 ]]; then PAQUETS=$LISTEPAQUETS; else PAQUETS=`renvoiepaquets $LISTEPAQUETS`; fi
my_selection_paquets_a_installer=$my_selection_paquets_a_installer" "$PAQUETS

TXTPAQUETS="Education et Jeux"
LISTEPAQUETS="gcompris tuxpaint tuxpaint-stamps-default tuxpaint-plugins-default scratch gnome-games gnome-chess minetest klavaro"
if [[ "$TEST_ALL" -eq 0 ]]; then PAQUETS=$LISTEPAQUETS; else PAQUETS=`renvoiepaquets $LISTEPAQUETS`; fi
my_selection_paquets_a_installer=$my_selection_paquets_a_installer" "$PAQUETS

TXTPAQUETS="Polices de caractères"
LISTEPAQUETS="fonts-crosextra-caladea fonts-crosextra-carlito fonts-linuxlibertine gsfonts-x11 fonts-materialdesignicons-webfont fonts-arkpandora fonts-entypo"
if [[ "$TEST_ALL" -eq 0 ]]; then PAQUETS=$LISTEPAQUETS; else PAQUETS=`renvoiepaquets $LISTEPAQUETS`; fi
my_selection_paquets_a_installer=$my_selection_paquets_a_installer" "$PAQUETS


###### Listes avec dépots PPA
PPA="libreoffice/ppa"
TXTPAQUETS="LibreOffice récent (avec dépot PPA)"
LISTEPAQUETS="libreoffice-l10n-fr libreoffice-help-fr libreoffice-style-human libreoffice-style-oxygen libreoffice-style-elementary libreoffice-style-sifr libreoffice-style-galaxy libreoffice-style-hicontrast openclipart-libreoffice tango-icon-theme"
if [[ "$TEST_ALL" -eq 0 ]]; then PAQUETS=$LISTEPAQUETS; else PAQUETS=`renvoiepaquets $LISTEPAQUETS`; fi
if [[ ! -z $PAQUETS ]]; then
	my_selection_ppa_a_installer=$my_selection_ppa_a_installer" "$PPA
fi
my_selection_paquets_a_installer=$my_selection_paquets_a_installer" "$PAQUETS

# Ajout des ppa si présents
for PPA_UNIQUE in $my_selection_ppa_a_installer:
do
    if [[ ! -z  $PPA_UNIQUE ]]; then
        echo "add-apt-repository -y ppa:"$PPA_UNIQUE >> $MAJIX_COMMANDE
    fi
done


# Installation manuelle de paquets supplémentaires
if [[ "$TEST_ALL" -eq 0 ]];
then
    PAQUETSMANUELS=ajoutpaquetsmanuels
    my_selection_paquets_a_installer=$my_selection_paquets_a_installer" "$PAQUETSMANUELS
fi

# Installation de paquets non-libres
TXTPAQUETS="Paquets NON-LIBRES : veuillez accepter leur licence !"
LISTEPAQUETS="libdvd-pkg ubuntu-restricted-extras gstreamer1.0-plugins-bad gstreamer1.0-plugins-ugly ttf-mscorefonts-installer"
if [[ "$TEST_FREE" -ne 0 ]]; then
    if [[ "$TEST_ALL" -eq 0 ]];
    then
	PAQUETS=$LISTEPAQUETS;	
    else
	yad --warning --image=warning --width=500 --title="Paquets non libres" --text="Les paquets suivants sont <span color='#FF0000'>non-libres</span> et nécessitent d'accepter leur <b>licence</b> respective." --button=gtk-ok:0 2>/dev/null
	PAQUETS=`renvoiepaquets $LISTEPAQUETS`
    fi
    my_selection_paquets_a_installer=$my_selection_paquets_a_installer" "$PAQUETS

fi


# Suppression des paquets inutiles
echo "apt-get $DEBUG -y purge $PURGE_PAQUETS_FR" >> $MAJIX_COMMANDE
echo "apt-get $DEBUG -y purge $PURGE_PAQUETS_FONTS" >> $MAJIX_COMMANDE

# Mise à jour des dépots & installation
echo "apt-get -y update" >> $MAJIX_COMMANDE
echo "apt-get $DEBUG -y install $my_selection_paquets_a_installer" >> $MAJIX_COMMANDE

TESTLIBDVD=`echo "$selection_paquets_a_installer" | grep "libdvd-pkg"`
if [[ -n $TESTLIBDVD ]]; then echo "dpkg-reconfigure libdvd-pkg" >> $MAJIX_COMMANDE ; fi

#Mise à jour
echo "apt-get $DEBUG -y upgrade " >> $MAJIX_COMMANDE
echo "apt-get $DEBUG -y dist-upgrade" >> $MAJIX_COMMANDE
#Nettoyage
echo "apt-get $DEBUG -y autoremove" >> $MAJIX_COMMANDE

confirmationfinale
#Validation avant application

finsortie
#exit !
